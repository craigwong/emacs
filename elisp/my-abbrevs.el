(clear-abbrev-table global-abbrev-table)

(define-abbrev-table 'global-abbrev-table
  '(
    ;; Star Trek
    ("llap" "Live long and Prosper" )
    ("gday" "Today is a good day to die" )
    ("adr" "I'm a doctor, not a..." )
    ("doit" "Make it so" )
    ("idic" "Infinite Diversity in Infinite Combinations" )
    ))

;; TODO: Use yes-or-no-p to prompt for which options to use
(define-skeleton sh-skeleton-bash-shebang
  "Insert Bash shebang"
  nil
  "#!/bin/bash -euxo pipefail" \n
  \n
  _
  )

(define-skeleton sh-skeleton-if
  "Insert if statement"
  nil
  "if " (skeleton-read "Condition: ") " then" \n
  > (skeleton-read "Statement: ") \n
  < "fi"
  )

;; TODO: Nested skeleton that prompts for condition format
(define-skeleton sh-skeleton-if-else
  "Insert if-else statement"
  nil
  "if " (skeleton-read "Condition: ") " then" \n
  > (skeleton-read "if-Statement: ") \n
  < "else" \n
  > (skeleton-read "else-Statement: ") \n
  < "fi"
  )

(define-skeleton sh-skeleton-if-elif-else
  "Insert if-elsif-else statement"
  nil
  "if " (skeleton-read "if-Condition: ") " then" \n
  > (skeleton-read "if-Statement: ") \n
  < "elif " (skeleton-read "elif-Condition: ") " then" \n
  > (skeleton-read "elif-Statement: ") \n
  < "else" \n
  > (skeleton-read "else-Statement: ") \n
  < "fi" \n
  )

(define-skeleton sh-skeleton-for
  "Insert for loop"
  nil
  "for " (skeleton-read "item: ") " in " (skeleton-read "list: ") "; do" \n
  > (skeleton-read "Statement: ") \n
  < "done" \n
  )

(define-skeleton sh-skeleton-while
  "Insert while loop"
  nil
  "while " (skeleton-read "Condition: ") "; do" \n
  > (skeleton-read "Statement: ") \n
  < "done" \n
  )

(define-skeleton sh-skeleton-while-read
  "Insert while read loop"
  nil
  "while read " (skeleton-read "Filename: ") "; do" \n
  > (skeleton-read "Statement: ") \n
  < "done" \n
  )

(define-skeleton sh-skeleton-until
  "Insert until loop"
  nil
  "until " (skeleton-read "Condition: ") "; do" \n
  > (skeleton-read "Statement: ") \n
  < "done" \n
  )

(define-skeleton sh-skeleton-case
  "Insert case statements"
  nil
  "case " (skeleton-read "Expression: ") " in" \n
  (skeleton-read "Case 1: ") ")" \n
  > (skeleton-read "Case 1 statement: ") \n
  > ";;" \n
  < (skeleton-read "Case 2: ") ")" \n
  > (skeleton-read "Case 2 statement: ") \n
  > ";;" \n
  < "*)" \n
  > (skeleton-read "Catch all statement: ") \n
  > ";;" \n
  < "esac"
  )

(when (boundp 'sh-mode-abbrev-table)
  (clear-abbrev-table sh-mode-abbrev-table))

(define-abbrev-table 'sh-mode-abbrev-table
  '(
    ("bash" "" sh-skeleton-bash-shebang 0)
    ("ift" "" sh-skeleton-if 0)
    ("ife" "" sh-skeleton-if-else 0)
    ("ifee" "" sh-skeleton-if-elif-else 0)
    ("floop" "" sh-skeleton-for 0)
    ("wloop" "" sh-skeleton-while 0)
    ("rloop" "" sh-skeleton-while-read 0)
    ("uloop" "" sh-skeleton-until 0)
    ("cas" "" sh-skeleton-case 0)
    ))

(set-default 'abbrev-mode t)

(setq save-abbrevs nil)
